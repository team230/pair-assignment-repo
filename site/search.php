<?php
	include 'included/top.inc';
	printTop('Search', '1', 'Search', '', '', $suburbs);
?>
			<div class="cInput"> <!-- start page form -->
				<fieldset>
					<h1>Search</h1>
					<form onsubmit="return valRating(minrating);" action="results.php" method="get"  target="_parent">
						<div class="column">
							<div id="geoError"></div>
							<?php
								// search form
								$error = array();
								pasteLabel(false, 'Name');
								pasteLabel(false, 'Suburb');
								pasteLabel(true, '');
								searchField(false, true, $error, 'name', 'Name...');
								suburbDropdown(false, $suburbs, false, 'suburb', $error);
							?>
							<div class="lButton">
								<a href="#" onClick="getPos();">Get Location</a>
							</div><br>
							<?php
								hiddenField('lat');
								hiddenField('lon');
								pasteLabel(false, '');
								pasteLabel(false, 'Minimum Rating');
								pasteLabel(true, '');
								checkBox('filterrating', 'lButton', 'filterrating', 'ratingDisabled(this);', 'Filter by ratings: ', false);
								rate(false, 'minrating', 'min', $error);
								pasteLabel(true, '');
							?>
							<button type="submit" value="advSearch">Advanced Search</button>
						</div>
					</form>
				</fieldset>
			</div><!-- end page form -->
<?php include 'included/end.inc'; ?>