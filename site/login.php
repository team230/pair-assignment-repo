<?php include 'included/top.inc'; printTop('Login', '1', 'Login', '', '', $suburbs); ?>
			<div class="cInput"> <!-- start page form -->
				<fieldset>
					<h1>Login</h1>
					<div>
						<?php
							require 'login/checkLogin.inc';
								
							if(isset($_SESSION['user'])){
								header("Location: index.php"); // redirect to main page if logged in
								exit();
							}
							$error = array();
							// if page posted
							if(isset($_POST['login'])){
								require 'login/validate.inc';
								$userName = $_POST['user'];
								$passWd = $_POST['pass'];
								
								// check login details
								if(checkLogin($error, $_POST['user'], $_POST['pass'], $pdo)){
									$_SESSION['user'] = $userName;
									header("Location: index.php");
								} else {
									echo "<div class=\"error\">";
									echo "Enter a valid username and password";
									echo "</div>";
								}
							}
						?>
					</div>
					<form method="post">
						<div class="column">
							<?php
								pasteLabel(true, 'Username');
								textField(true, true, $error, 'user', 'Username');
								pasteLabel(true, 'Password');
								textPassword(true, $error, 'pass', 'Password');
								// password not posted back for security
							?>
							<button type="submit" name="login" value="login">Login</button>
						</div>
					</form>
					<p>or</p>
					<form action="register.php" target="_parent">
						<button type="submit" value="register">Register</button>
					</form>
				</fieldset>
			</div><!-- end page form -->
			<?php include 'included/end.inc'; ?>