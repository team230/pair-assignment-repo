<?php
	if(isset($_SESSION['user'])){
		header("Location: index.php");
	}
	include 'included/top.inc'; 
?>
<?php printTop('Register', '1', 'Register', '', '', $suburbs); ?>
			<div class="cInput"> <!-- start page form -->
				<fieldset>
				<h1>Register</h1>
				<?php
					if(isset($_SESSION['user'])){
						header("Location: index.php"); // redirect to main page if logged in
						exit();
					}
				?>
				<form onsubmit="return checkPassword(confirmpass);" method="post">
					<div class="column">
						<?php
							$error = array();
							if(isset($_POST['register'])) {
								require 'login/validate.inc';
								textValidate($error, $_POST, 'username');
								emailValidate($error, $_POST, 'email');
								dateValidate($error, $_POST, 'dob');
								phoneValidate($error, $_POST, 'phone');
								match($error, $_POST, 'password', 'confirmpass');
								isRequired($error, $_POST, 'password');
								isRequired($error, $_POST, 'suburb');
								isRequired($error, $_POST, 'gender');
								isRequired($error, $_POST, 'dob');
								isRequired($error, $_POST, 'phone');
								userExists($error, $_POST, 'username', $pdo);
								//echo $_POST['sendMail'];
								
								if($error){
									echo "<div class=\"error\">";
									echo "Some Fields Incorrect: <br>";
									foreach ($error as $name => $error){
										echo "$name$error <br>";
									}
									echo "</div>";
								} if(!$error) {
									$salt = uniqid(mt_rand(), true);
									if(isset($_POST['sendMail'])){
										$sendmail=true;
									} else {
										$sendmail=false;
									}
									$gender = $_POST['gender'];
									$email = $_POST['email'];
									$username = $_POST['username'];
									$suburb = $_POST['suburb'];
									$password = $_POST['password'];
									$dob = $_POST['dob'];
									$phone = $_POST['phone'];
									$crypt = $password.$salt;
									
									// insert new user in database
									try
									{
										$username;
										$data = $pdo->prepare("INSERT INTO members (username, email, gender, password, salt, sendmail, dob, phone) VALUES (:username, :email, :gender, SHA2(:crypt, 0), :salt, :sendmail, :dob, :phone)");
										$data->bindValue(':username', $username);
										$data->bindValue(':email', $email);
										$data->bindValue(':gender', $gender);
										$data->bindValue(':crypt', $crypt);
										$data->bindValue(':salt', $salt);
										$data->bindValue(':sendmail', $sendmail, PDO::PARAM_BOOL);
										$data->bindValue(':dob', $dob);
										$data->bindValue(':phone', $phone);
										$data->execute();
									}
									catch (PDOException $e)
									{
									echo $e->getMessage();
									}
									header('Refresh:1; url=login.php');
									echo "<div class=\"confirm\">";
									echo "Successfully registered, moving to login...";
									echo "</div>";
								}
							}
							
							// user registration form
							pasteLabel(false, 'Username');
							pasteLabel(true, 'Email');
							textField(false, true, $error, 'username', 'Username');
							echo " ";
							emailField(true, false, $error, 'email', 'email@example.com');
							pasteLabel(false, 'Password');
							pasteLabel(true, 'Re-type Password');
							textPassword(false, $error, 'password', 'Password');
							echo " <input type=\"password\" name=\"confirmpass\" id=\"confirmpass\" class=\"textbox\" placeholder=\"Re-Type Password\" oninput=\"checkPassword(this)\" required /><br>";
							pasteLabel(false, 'Preferred Suburb');
							pasteLabel(true, 'Gender');
							suburbDropdown(false, $suburbs, true, 'suburb', $error);
							echo " ";
							twoRadio($error, 'radButtons', 'gender', 'male', 'female');
							pasteLabel(false, 'Date of Birth');
							pasteLabel(true, 'Phone');
							dateField(false, false, $error, 'dob', 'DD/MM/YYYY');
							echo " ";
							phoneField(true, false, $error, 'phone', '07 6666 6666');
							checkBox('sendMail', 'lButton', 'sendMail', '', 'Send me emails about new locations', true);
							
						?>
					</div>
					<button type="submit" name="register" value="register">Register</button>
				</form></fieldset>
			</div><!-- end page form -->
			<?php include 'included/end.inc'; ?>