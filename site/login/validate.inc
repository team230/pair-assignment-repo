<?php
	function emailValidate(&$error, $field, $name)
	{
		// based on email validation from tutorial
		$pattern = '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/';
		if(!isset($field[$name])|| empty($field[$name]))
			$error[$name] = 'Field is required.';
		if(!preg_match($pattern,$field[$name]))
			$error[$name] = 'Email is not correct format.';
		return true;
	}
	function textValidate(&$error, $field, $name)
	{
		if(empty($field[$name]))
			$errors[$name] = 'Field cannot be empty';
		if(!preg_match("/^[0-9a-zA-Z]+$/", $field[$name])){
			$errors[$name] = 'Please use a-z characters.';
		}
		return true;
	}
	
	function phoneValidate(&$error, $field, $name)
	{
		if(empty($field[$name]))
			$errors[$name] = 'Field cannot be empty';
		if(!preg_match("/^(07|61)[ ]?\d{4}[ ]?\d{4}$/", $field[$name])){
			$errors[$name] = 'Please enter a valid phone number.';
		}
		return true;
	}
	
	function dateValidate(&$error, $field, $name)
	{  
		(int) $dayMonth = explode('/', $field[$name]."/");
		/* echo "YEAR: ".$dayMonth[2]."<br>";
		echo "DAY: ".$dayMonth[0]."<br>";
		echo "MONTH: ".$dayMonth[1]."<br>"; */ //debugging
		if(checkdate($dayMonth[1],$dayMonth[0],$dayMonth[2])){
			//echo "YES";
		} else {
			$errors[$name] = 'Please enter a valid date.';
		}
		if(empty($field[$name]))
			$errors[$name] = 'Field cannot be empty';
		if(!checkdate($dayMonth[1],$dayMonth[0],$dayMonth[2])){
			$errors[$name] = 'Please enter a valid date.';
		}
		return true;
	}
	
	function isRequired(&$error, $field, $name)
	{
		if(!isset($field[$name]))
			$error[$name] = 'Field is required.';
		return true;
	}
	function match(&$error, $field, $name1, $name2)
	{
		if(!$field[$name1] == $field[$name2])
			$error[$name1] = 'Fields do not match.';
		return true;
	}
	function userExists(&$error, $field, $name, $pdo){
		try
		{
			$username = $field[$name];
			$query = $pdo->prepare('SELECT * FROM members WHERE username = :username');
			$query->bindValue(':username', $username);
			$query->execute();
		}
		catch (PDOException $e)
		{
		echo $e->getMessage();
		}
		
		if($query->rowCount() != 0){
			$error[$username] = ': Username is already taken';
		} else if (!preg_match("/^[0-9a-zA-Z]+$/", $field[$name])){
			$error[$username] = ': Username must be numbers or upper and lowercase letters a-z.';
		} else {
			return true;
		}
	}
?>