<?php
	function checkLogin(&$error, $username, $password, $pdo){
		try
		{
			$query = $pdo->prepare('SELECT * FROM members WHERE username = :username and password = SHA2(CONCAT(:password, salt), 0)');
			$query->bindValue(':username', $username);
			$query->bindValue(':password', $password);
			$query->execute();
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
		}
		
		if($query->rowCount() > 0){
			return true;
		} else {
			return false;
		}
	}
?>