<?php
	function executeSearch($identifier ,$name, $suburb, $lat, $lon, $filterrating, $minrating, $orderBy, $pdo){
		// include reviews in the result
		$sql = "SELECT * FROM (SELECT AVG(coalesce(reviews.Rating)) as avgrating, items.*
		FROM items
		LEFT JOIN reviews ON items.ID = reviews.locationID
		GROUP BY items.ID) AS combined ";
		
		try {
			$build = false;
			$whered = false;
			$where = " WHERE ";
			$and = " AND ";
			$end = " ORDER BY $orderBy ";
			if(isset($name) && $name != ''){
				// add name to results
				$nameQuery = " combined.name LIKE :name ";
				$build = true;
			}
			if(isset($suburb) && $suburb != ''){
				// add suburb to query
				$suburbQuery = " combined.suburb LIKE :suburb ";
				$build = true;
			}
			if(isset($identifier) && $identifier != '' && preg_match("/^[0-9a-zA-Z]+$/", $identifier)){
				// add suburb to query
				$identifierQuery = " combined.ID = :identifier ";
				$build = true;
			}
			if(isset($filterrating) && $filterrating != ''){
				// add minrating to results
				if(isset($minrating) && $minrating != ''){
					$build = true;
					$ratingQuery = " combined.avgrating >= :minrating ";
				}
			}
			
			// assemble query
			if($build){
				if(isset($nameQuery)){
					if(!$whered){
						$whered = true;
						$sql = $sql . $where;
					}
					$sql = $sql . $nameQuery;
				}
				
				if(isset($suburbQuery)){
					if(!$whered){
						$whered = true;
						$sql = $sql . $where;
						$sql = $sql . $suburbQuery;
					} else {
						$sql = $sql . $and . $suburbQuery;
					}
				}
				
				if(isset($identifierQuery)){
					if(!$whered){
						$whered = true;
						$sql = $sql . $where;
						$sql = $sql . $identifierQuery;
					} else {
						$sql = $sql . $and . $identifierQuery;
					}
				}
				
				if(isset($ratingQuery)){
					if(!$whered){
						$whered = true;
						$sql = $sql . $where;
						$sql = $sql . $ratingQuery;
					} else {
						$sql = $sql . $and . $ratingQuery;
					}
				}
			}
			$sql = $sql . $end;
			$search = $pdo->prepare($sql);
			// bind values
			if(isset($nameQuery)){
				$search->bindValue(':name', '%'.$name.'%');
				//echo "A BOUND";
			}		
			if(isset($suburbQuery)){
				$search->bindValue(':suburb', '%'.$suburb.'%');
				//echo "B BOUND";
			}
			if(isset($identifierQuery)){
				$search->bindValue(':identifier', $identifier, PDO::PARAM_INT);
				//echo "B BOUND";
			}
			if(isset($ratingQuery)){
				$search->bindValue(':minrating', '%'.$minrating.'%');
				//echo "C BOUND";
			}
			// echo "<br>".$sql."<br>"; // for testing
			$search->execute();
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
		
		return $search;
	}
	
	function drawResult($ID, $name, $rating, $suburb, $address, $lat, $lon){
		// draw a textual result on the page
		echo "<div class=\"result\">";
		echo "<h1>$name<span style=\"float:right;\">";
		if($rating == ''){
			echo "Not Rated";
		} else {
			echo "Rating: ".number_format($rating, 1, '.', ',')." / 5";
		}
		echo "</span></h1>";
		echo "<p><b>Address:</b> $address, $suburb<br />";
		echo "<a href =\"itemdetail.php?ID=$ID\"> More details ...</a></p>";
		echo "</div>";
	}
	
	function getUserByID($pdo, $userID){
		// given a user ID get the username
		try
		{
			$userArray = array();
			$query = $pdo->prepare('SELECT * FROM members WHERE userID = :userID');
			$query->bindValue(':userID', $userID, PDO::PARAM_INT);
			$query->execute();
			$userArray = $query->fetchAll();
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
		}
		return $userArray;
	}
	
	function getUserByUser($pdo, $user){
		// given a username return the ID of the user
		try
		{
			$query = $pdo->prepare('SELECT * FROM members WHERE username = :username');
			$query->bindValue(':username', $user);
			$query->execute();
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
		}
		return $query;
	}
	
	function calcDistance($lat1, $lon1, $lat2, $lon2){
		// based on the distance example from workshop 5 but is a php equivalent
		$pi = 0.017453292519943295;    // Math.PI / 180
		$a = 0.5 - cos(($lat2 - $lat1) * $pi)/2 + cos($lat1 * $pi) * cos($lat2 * $pi) * (1 - cos(($lon2 - $lon1) * $pi))/2;
		
		$distance = 12742 * asin(sqrt($a));
		return $distance;
	}
?>