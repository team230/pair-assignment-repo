<?php include 'included/top.inc'; printTop('Results', '2', 'Search', 'search.php', 'Results', $suburbs); ?>
			<?php
				include 'result/executeSearch.inc';
				
				// set default values
				$name = '';
				$suburb = '';
				$lat = '';
				$lon = '';
				$filterrating = '';
				$minrating = '';
				$orderBy = 'combined.avgrating DESC'; // default sorting
				
				// translate get values, overrride defaults above
				if(isset($_GET['name'])){
					$name = $_GET['name'];
				}
				if(isset($_GET['suburb'])){
					$suburb = $_GET['suburb'];
				}
				if(isset($_GET['lat'])){
					$lat = $_GET['lat'];
				}
				if(isset($_GET['lon'])){
					$lon = $_GET['lon'];
				}
				if(isset($_GET['filterrating'])){
					$filterrating = $_GET['filterrating'];
				}
				if(isset($_GET['minrating'])){
					$minrating = $_GET['minrating'];
				}
				if(isset($_GET['orderBy'])){
					$orderBy = $_GET['orderBy'];
				}
				$searchArray = array();
				$modifyArray = array();
				$searchResults = executeSearch('' ,$name, $suburb, $lat, $lon, $filterrating, $minrating, $orderBy, $pdo);
				$modifyArray = $searchResults->fetchAll();
				
				
				// if lat an lon numbers entered filter results
				if(is_numeric($lat) && is_numeric($lon)){
					foreach($modifyArray as $res){
						//echo "DISTANCE CALC: ".calcDistance($lat, $lon, $res['latitude'], $res['longitude']); // for testing
						if(calcDistance($lat, $lon, $res['latitude'], $res['longitude']) < 5){
							array_push($searchArray,$res);
						}
					}
				} else {
					$searchArray = $modifyArray;
				}
				$rows = 0;
			?>
			<div id="mapid2"></div><br>
			<script>
				displayResultMap(<?php echo json_encode($searchArray) ?>);
			</script>
			<?php
				// draw all the results
				foreach ($searchArray as $result){
					drawResult($result['ID'], $result['name'], $result['avgrating'], $result['suburb'], $result['address'], $result['latitude'], $result['longitude']);
					$rows++;
				}
				if($rows == 0){
					echo "<center>No Results Found</center>";
				}
			?>
<?php include 'included/end.inc'; ?>