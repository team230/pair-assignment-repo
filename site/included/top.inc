<?php include 'login/session.inc'; ?>
<!DOCTYPE html>
<html>
<head>
	
<?php
	include 'forms/formCreate.inc';
	function printTop($title, $levels, $name1, $link1, $name2, &$suburbs){
		echo "<title>".$title."</title>";
		echo '<link href="css/format.css" rel="stylesheet" type="text/css"/>
				<link href="css/forms.css" rel="stylesheet" type="text/css"/>
				<link href="css/search.css" rel="stylesheet" type="text/css"/>
				<script type="text/javascript" src="js/myscripts.js"></script>
				<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
				<script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
				<!-- meta data -->
				<meta charset="UTF-8">
				<meta name="description" content="WiFi2GO finding the wifi for you in Brisbane">
				<meta name="keywords" content="WiFi, WiFi2GO, Brisbane, ">
				<meta name="author" content="Kyle, Claudia">
			</head>
			<body>
			<div class="sContainer"><!-- Search bar/header, start -->';
	include 'searchbar.inc';
	echo '</div> <!-- end search bar -->
	
	<div id="wrapper">
		<div id="menu"><!-- breadcrumbs -->';
		include 'included/menu.inc';
		if($levels == '1'){
			drawMenuOne($name1);
		} elseif ($levels == '2') {
			drawMenuTwo($name1, $link1, $name2);
		} elseif ($levels == '3') {
			drawMenuBack($name1, $name2);
		}
		echo '	
			
		</div>
		
		<div id="content"><!-- main page content -->';
	}
?>
