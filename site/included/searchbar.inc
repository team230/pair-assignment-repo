<div id="theTitle">
	<h1>WiFi 2GO</h1>
	<p>Find the wifi you <i>deserve</i></p>
</div>
<div id="searchBar">
	<form action="results.php"  method="get" target="_parent">
		<?php
			$error = array();
			suburbDropdown(false, $suburbs, false, 'suburb', $error)
		?>
		<input type="search" name="name" class="textbox" placeholder="Name...">
		<button type="submit">Search</button>
	</form>
	<?php include 'login/loginButtons.inc'; ?>
	<a href="search.php">Advanced Search</a>
</div>