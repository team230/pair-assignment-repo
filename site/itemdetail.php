<?php include 'included/top.inc';
	include 'result/singleReview.inc';
	include 'result/executeSearch.inc';
	if(empty($_GET['ID']) || !preg_match("/^[0-9]+$/", $_GET['ID'])){
		header("Location: result/invalidLocation.php");
	}
	$locationID = $_GET['ID'];
	// echo $locationID; // for testing
	// if found rows != 1 redirect as above
	$itemDetails = executeSearch($locationID ,'', '', '', '', '', '', 'combined.avgrating DESC', $pdo);
	if($itemDetails->rowCount() != 1){
		header("Location: result/invalidLocation.php");
	}
	printTop('Location', '3', 'Results', '', 'Location', $suburbs);
	$item = array();
	$item = $itemDetails->fetchAll();
	$error = array();
	if(isset($_POST['review'])){
		require 'login/validate.inc';
		// validate entries from review
		textValidate($error, $_POST, 'title');
		textValidate($error, $_POST, 'rating');
		textValidate($error, $_POST, 'textBody');
		isRequired($error, $_POST, 'title');
		isRequired($error, $_POST, 'rating');
		isRequired($error, $_POST, 'textBody');
		
		$userData = getUserByUser($pdo, $_SESSION['user']);
		$user = $userData->fetchAll();
		if($error){
			// display errors from review
			echo "<div class=\"error\">";
			echo "Some Fields Incorrect: <br>";
			foreach ($error as $name => $error){
				echo "$name$error <br>";
			}
			echo "</div>";
		}
		
		if(!$error){
			$title = $_POST['title'];
			$rating = $_POST['rating'];
			$textContent = $_POST['textBody'];
			$local = $locationID;
			$userID = $user[0]['userID'];
			
			// attempt to insert review
			try
			{
				$data = $pdo->prepare("INSERT INTO reviews (locationID, postingUID, title, textContent, Rating) VALUES (:locationID, :postingUID, :title, :textContent, :Rating)");
				$data->bindValue(':locationID', $local);
				$data->bindValue(':postingUID', $userID);
				$data->bindValue(':title', $title);
				$data->bindValue(':textContent', $textContent);
				$data->bindValue(':Rating', $rating);
				$data->execute();
			}
			catch (PDOException $e)
			{
			echo $e->getMessage();
			}
			// refresh the page to display review
			header('Refresh:1; url=itemdetail.php?ID='.$locationID);
			echo "<div class=\"confirm\">";
			echo "Review Submitted!";
			echo "</div>";
		}
	}
?>
				<div itemscope itemtype="http://schema.org/Place">
					<div id="sidebox">
						<h1>Rating:</h1>
						<h2><?php echo number_format($item[0]['avgrating'], 1, '.', ','); ?>/5</h2>
					</div>
					<div id="info">
						<h1><span itemprop="name"><?php echo $item[0]['name']; ?></span></h1>
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<p><b>Address:</b> <span itemprop="streetAddress"><?php echo $item[0]['address'].", ".$item[0]['suburb']; ?></span><br />
						</div>
					</div>
					<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
						<meta itemprop="latitude" content="<?php echo $item[0]['latitude']; ?>">
						<meta itemprop="longitude" content="<?php echo $item[0]['longitude']; ?>">
					</div>
				
					<div id="item">
						<div id="mapid"></div><br>
						<script>
							displayItemMap(<?php echo json_encode($item) ?>);
						</script>
						<?php
							// draw the review input if user is logged in or display link to login
							if(isset($_SESSION['user']) && !empty($_SESSION['user'])){
								// echo "YOU ARE LOGGED IN"; //testing
								echo "<div class=\"reviewInput\">";
								echo "<form method=\"post\">";
								textField(false, true, $error, 'title', 'Title');
								echo " ";
								rateNormal(false, 'rating', 'Rating', $error);
								echo "<br>";
								reviewText(false, 'textBody', $error);
								echo "<div class=\"cInput\"><button type=\"submit\" name=\"review\" value=\"review\">Submit Review</button></div>";
								echo "</form>";
								echo "</div>";
							} else {
								echo "<a href=\"login.php\">Login to write reviews</a>";
							}
						?>
					</div>
					<?php
						// only display reviews if there exists reviews
						if(!empty($item[0]['avgrating'])){
							printReviews($locationID, $pdo, number_format($item[0]['avgrating'], 1, '.', ','));
						}
					?>
				</div>
				<div id="clearboxright"></div>
				<div id="clearboxleft"></div>
<?php include 'included/end.inc'; ?>