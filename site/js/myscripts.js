function checkPassword(type){
	// if passwords dont match show html error
	var mystring = document.getElementById('password').value; 
    if(type.value != mystring) {
        type.setCustomValidity('Password does not match');
        return false;
    } else {
		type.setCustomValidity('');
        return true;
    }
}

function ratingDisabled(check) {
	// if the rating dropdown is checked as disabled
	if(check.checked){
		document.getElementById('minrating').disabled=0;
		document.getElementById('maxrating').disabled=0;
	} else {
		document.getElementById('minrating').disabled=1;
		document.getElementById('maxrating').disabled=1;
	}
}

function valRating(minrate) {
	// check rating satisfies minimum rating
	var mystring = document.getElementById('maxrating').value;
	if(mystring < minrate.value){
		minrate.setCustomValidity('Minimum value is above max');
        return false;
	} else {
		minrate.setCustomValidity('');
        return true;
	}
}

function getPos() {
	// get location on click
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError);
	} else {
		document.getElementById("geoError").innerHTML="Geolocation is not supported by this browser.";
	}
}
function showPosition(position) {
	// insert location into the hidden fields
	document.getElementById("lat").value = position.coords.latitude;
	document.getElementById("lon").value = position.coords.longitude;	
}

function showError(error) {
	// method for handling the errors from geolocation
	var msg = "";
	switch(error.code) {
		case error.PERMISSION_DENIED:
			msg = "User denied the request for Geolocation."
			break;
		case error.POSITION_UNAVAILABLE:
			msg = "Location information is unavailable."
			break;
		case error.TIMEOUT:
			msg = "The request to get user location timed out."
			break;
		case error.UNKNOWN_ERROR:
			msg = "An unknown error occurred."
			break;
	}
	// print errors to error div on search page
	document.getElementById("geoError").innerHTML = msg;
	document.getElementById("geoError").style.color = "red";
	document.getElementById("geoError").style.fontSize = "75%";
}

function displayResultMap(items){
	// center on first result
	var firstLon = items[0]['longitude'];
	var firstLat = items[0]['latitude'];
	//window.alert(firstLon); // debugging
	//window.alert(firstLat);
	var mymap = L.map('mapid2').setView([firstLat, firstLon], 13);
	
	// add tile layer to map
	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
		maxZoom: 18
	}).addTo(mymap);
	
	// first marker made and popup opened
	L.marker([parseFloat(items[0]['latitude']), parseFloat(items[0]['longitude'])]).addTo(mymap)
		.bindPopup(items[0]['name']+"<br>"+"<a href=\"itemdetail.php?ID="+parseInt(items[0]['ID'])+"\""+">"+"More Information"+"</a>").openPopup();
	
	// add markers for other locations
	for (var i = 1; i < items.length; i++){
		L.marker([parseFloat(items[i]['latitude']), parseFloat(items[i]['longitude'])]).addTo(mymap)
		.bindPopup(items[i]['name']+"<br>"+"<a href=\"itemdetail.php?ID="+parseInt(items[i]['ID'])+"\""+">"+"More Information"+"</a>");
	}
}

function displayItemMap(items){
	// set values for single item
	var lon = items[0]['longitude'];
	var lat = items[0]['latitude'];
	var name = items[0]['name'];
	var address = items[0]['address'];
	var suburb = items[0]['suburb'];
	var mymap = L.map('mapid').setView([lat, lon], 13);
	
	// tile layer for map terrain images
	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
		maxZoom: 18
	}).addTo(mymap);
	// add marker
	var marker = L.marker([lat, lon]).addTo(mymap);
	marker.bindPopup("<b>"+name+"</b><br>"+address+"<br>"+suburb).openPopup();
}