<?php include 'login/session.inc'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Wifi2GO</title>
	<link href="css/format.css" rel="stylesheet" type="text/css"/>
	<link href="css/main.css" rel="stylesheet" type="text/css"/>
	<link href="css/search.css" rel="stylesheet" type="text/css"/>
	<!-- meta data -->
	<meta charset="UTF-8">
	<meta name="description" content="WiFi2GO finding the wifi for you in Brisbane">
	<meta name="keywords" content="WiFi, WiFi2GO, Brisbane, ">
	<meta name="author" content="Kyle, Claudia">
</head>
<body>
	<div class="sContainer"><!-- Search bar/header, start -->
		<div id="backing">
			<div id="theTitle">
				<h1>WiFi 2GO</h1>
				<p>Find the wifi you <i>deserve</i></p>
			</div><br>
			<div id="searchBar">
				<form action="results.php"  target="_parent">
					<?php
						$error = array();
						include 'forms/formCreate.inc';
						suburbDropdown(false, $suburbs, false, 'suburb', $error);
					?>
					<input type="search" name="name" class="textbox" placeholder="Name..." autofocus>
					<button type="submit" name="search">Search</button>
				</form>
				<a href="search.php">Advanced Search</a><br>
				<?php include 'login/loginButtons.inc'; ?>
			</div>
		</div>
	</div> <!-- end search bar -->
	
	<div id="wrapper">	
		<div id="content"><!-- start page content -->
		<br><p>WiFi around brisbane is our focus so we have collated some of the best places around you can get wifi.</p>
<?php include 'included/end.inc'; ?>