<?php
	// get the value that was posted
	function posted_value($name)
	{
		if(isset($_POST[$name]))
			return htmlspecialchars($_POST[$name]);
	}
	// if a break is needed print one
	function needBreak($break){
		if($break){
			echo "<br>";
		}
	}
	// generic textfield that takes numbers letters and spaces
	function textField($break, $auto, $error, $name, $placeholder)
	{
		$value = posted_value($name);
		echo "<input type=\"text\" pattern=\"[0-9a-zA-Z- ]+\" class=\"textbox\" placeholder=\"$placeholder\" id=\"$name\" name=\"$name\" value=\"$value\" onchange=\"this.setCustomValidity('')\" oninvalid=\"this.setCustomValidity('Please enter a value, in a-Z characters and numbers.')\" required ";
		if($auto == true){
			echo "autofocus";
		}
		echo ">";
		if($break == true){
			echo '<br>';
		}
	}
	// field for a phone number
	function phoneField($break, $auto, $error, $name, $placeholder)
	{
		// typical format from google: ‎(0x) xxxx xxxx
		$pattern = "(07|61)[ ]?\d{4}[ ]?\d{4}";
		$value = posted_value($name);
		echo "<input type=\"text\" pattern=\"$pattern\" class=\"textbox\" placeholder=\"$placeholder\" id=\"$name\" name=\"$name\" value=\"$value\" onchange=\"this.setCustomValidity('')\" oninvalid=\"this.setCustomValidity('Please enter a phone number, in format 07 1234 5678 or 61 1234 5678')\" required ";
		if($auto == true){
			echo "autofocus";
		}
		echo ">";
		if($break == true){
			echo '<br>';
		}
	}
	// field for dates
	function dateField($break, $auto, $error, $name, $placeholder)
	{
		// typical format DD/MM/YYYY
		$pattern = "(0[1-9]|1[0-9]|2[0-9]|3[01])[/](1[012]|0[1-9])[/]([0-9]{4})";
		$value = posted_value($name);
		echo "<input type=\"text\" pattern=\"$pattern\" class=\"textbox\" placeholder=\"$placeholder\" id=\"$name\" name=\"$name\" value=\"$value\" onchange=\"this.setCustomValidity('')\" oninvalid=\"this.setCustomValidity('Please enter a date, in format DD/MM/YYYY')\" required ";
		if($auto == true){
			echo "autofocus";
		}
		echo ">";
		if($break == true){
			echo '<br>';
		}
	}
	// field used for search less specifications than textfield
	function searchField($break, $auto, $error, $name, $placeholder)
	{
		$value = posted_value($name);
		echo "<input type=\"text\" class=\"textbox\" placeholder=\"$placeholder\" id=\"$name\" name=\"$name\" value=\"$value\" ";
		if($auto == true){
			echo "autofocus";
		}
		echo ">";
		if($break == true){
			echo '<br>';
		}
	}
	// field for email
	function emailField($break, $auto, $error, $name, $placeholder)
	{
		$value = posted_value($name);
		echo "<input type=\"email\" class=\"textbox\" placeholder=\"$placeholder\" id=\"$name\" name=\"$name\" value=\"$value\" required ";
		if($auto == true){
			echo "autofocus";
		}
		echo ">";
		needBreak($break);
	}
	// password field
	function textPassword($break, $error, $name, $placeholder)
	{
		echo "<input type=\"password\" class=\"textbox\" placeholder=\"$placeholder\" id=\"$name\" name=\"$name\" required>";
		needBreak($break);
	}
	// heading for fields
	function pasteLabel($break, $text)
	{
		echo "<p class=\"boxNames\">$text</p>";
		if($break == true){
			echo '<br>';
		}
	}
	// suburb selection box
	function suburbDropdown($break, $suburbs, $required, $name, $error)
	{
		$value = posted_value($name);
		echo "<select name=\"suburb\"";
		if($required){
			echo " required ";
		}
		echo ">";
		// if there is a posted value select that instead above
		if(!isset($value)){
			echo "<option disabled selected value> Select a suburb </option>";
		}
		// populate the suburb selection
		foreach ($suburbs as $suburb){
			echo '<option value="'.$suburb['suburb'].'"';
			if(isset($value) && $value == $suburb['suburb']){
				echo " selected ";
			}
			echo '>'.$suburb['suburb'].'</option>';
		}
		echo "</select>";
		needBreak($break);
	}
	// dual radio group supports posted values
	function twoRadio($error, $divId, $name, $value1, $value2)
	{
		$value = posted_value($name);
		echo "<div id=\"$divId\">";
		echo "<input type=\"radio\" name=\"$name\" value=\"$value1\" required";
		if(isset($value) && $value == $value1){
			echo " checked ";
		}
		echo "> $value1 <input type=\"radio\" name=\"$name\" value=\"$value2\"";
		if(isset($value) && $value == $value2){
			echo " checked ";
		}
		echo "> $value2 ";
		echo "</div><br>";
	}
	// checkbox with onclick
	function checkBox($name, $id, $value1, $onClick, $text, $break)
	{
		$value = posted_value($name);
		echo "<div class=\"$id\">";
		echo "		<input type=\"checkbox\" name=\"$name\" value=\"$value1\" onClick=\"$onClick\"";
		if(isset($value)){
			echo " checked ";
		}
		echo "> $text ";
		echo "</div>";
		needBreak($break);
	}
	// generate a 1 - 5 rating selection box
	function rate($break, $name, $text, $error)
	{
		$value = posted_value($name);
		echo "<select name=\"$name\" id=\"$name\" oninput=\"return valRating(minrating);\" disabled required>";
		echo "<option disabled";
		if(!isset($value)){
			echo " selected ";
		}
		echo "value> $text </option>";
		
		for($i = 1; $i <= 5; $i++){
			echo "<option ";
			if($value == $i){
				echo " selected ";
			}
			echo "value=\"$i\"> $i </option>";
		}
		
		echo "</select>";
		needBreak($break);
	}
	// rating that is simply required
	function rateNormal($break, $name, $text, $error)
	{
		$value = posted_value($name);
		echo "<select name=\"$name\" id=\"$name\" required>";
		echo "<option disabled";
		if(!isset($value)){
			echo " selected ";
		}
		echo "value> $text </option>";
		
		for($i = 1; $i <= 5; $i++){
			echo "<option ";
			if($value == $i){
				echo " selected ";
			}
			echo "value=\"$i\"> $i </option>";
		}
		
		echo "</select>";
		needBreak($break);
	}
	// basic hidden field
	function hiddenField($name)
	{
		$value = posted_value($name);
		echo "<input type=\"hidden\" name=\"$name\" id=\"$name\"";
		if(isset($value)){
			echo " value=\"$value\"";
		}
		echo ">";
	}
	// text entry for reviews
	function reviewText($break, $name, $error)
	{
		$value = posted_value($name);
		echo "<textarea rows=\"3\" cols=\"50\"";
		if(isset($value)){
			echo " value=\"$value\"";
		}
		echo " name=\"$name\" placeholder=\"Maximum of 300 characters\" maxlength=\"300\" required></textarea>";
		needBreak($break);
	}
?>