-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: hotspots
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `suburb` varchar(256) DEFAULT NULL,
  `latitude` decimal(20,10) DEFAULT NULL,
  `longitude` decimal(20,10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'7th Brigade Park, Chermside','Delaware St','Chermside',-27.3789300000,153.0446100000),(2,'Annerley Library Wifi','450 Ipswich Road','Annerley, 4103',-27.5094228500,153.0333218000),(3,'Ashgrove Library Wifi','87 Amarina Avenue','Ashgrove, 4060',-27.4439462900,152.9870981000),(4,'Banyo Library Wifi','284 St. Vincents Road','Banyo, 4014',-27.3739664100,153.0783234000),(5,'Booker Place Park','Birkin Rd & Sugarwood St','Bellbowrie',-27.5635300000,152.8937200000),(6,'Bracken Ridge Library Wifi','Corner Bracken and Barrett Street','Bracken Ridge, 4017',-27.3179726100,153.0378735000),(7,'Brisbane Botanic Gardens','Mt Coot-tha Rd','Toowong',-27.4772400000,152.9759900000),(8,'Brisbane Square Library Wifi','Brisbane Square, 266 George Street','Brisbane, 4000',-27.4709117300,153.0224598000),(9,'Bulimba Library Wifi','Corner Riding Road & Oxford Street','Bulimba, 4171',-27.4520308600,153.0628242000),(10,'Calamvale District Park','Formby & Ormskirk Sts','Calamvale',-27.6215200000,153.0366500000),(11,'Carina Library Wifi','Corner Mayfield Road & Nyrang Street','Carina, 4152',-27.4916931400,153.0912127000),(12,'Carindale Library Wifi','The Home and Leisure Centre, Corner Carindale Street  & Banchory Court, Westfield Carindale Shopping Centre','Carindale, 4152',-27.5047592800,153.1003965000),(13,'Carindale Recreation Reserve','Cadogan and Bedivere Sts','Carindale',-27.4970000000,153.1110500000),(14,'Chermside Library Wifi','375 Hamilton  Road','Chermside, 4032',-27.3856032000,153.0349028000),(15,'City Botanic Gardens Wifi','Alice Street','Brisbane City',-27.4756100000,153.0300500000),(16,'Coopers Plains Library Wifi','107 Orange Grove Road','Coopers Plains, 4108',-27.5651050900,153.0403183000),(17,'Corinda Library Wifi','641 Oxley Road','Corinda, 4075',-27.5388023700,152.9809091000),(18,'D.M. Henderson Park','Granadilla St','MacGregor',-27.5774500000,153.0760300000),(19,'Einbunpin Lagoon','Brighton Rd','Sandgate',-27.3194700000,153.0682200000),(20,'Everton Park Library Wifi','561 South Pine Road','Everton park, 4053',-27.4053336000,152.9904205000),(21,'Fairfield Library Wifi','Fairfield Gardens Shopping Centre, 180 Fairfield Road','Fairfield, 4103',-27.5090903800,153.0259709000),(22,'Forest Lake Parklands','Forest Lake Bld','Forest Lake',-27.6202000000,152.9662500000),(23,'Garden City Library Wifi','Garden City Shopping Centre, Corner Logan and Kessels Road','Upper Mount Gravatt, 4122',-27.5624422100,153.0809183000),(24,'Glindemann Park','Logan Rd','Holland Park West',-27.5255200000,153.0692300000),(25,'Grange Library Wifi','79 Evelyn Street','Grange, 4051',-27.4253119300,153.0174728000),(26,'Gregory Park','Baroona Rd','Paddington',-27.4670000000,152.9998100000),(27,'Guyatt Park','Sir Fred Schonell Dve','St Lucia',-27.4929700000,153.0018700000),(28,'Hamilton Library Wifi','Corner Racecourt Road and Rossiter Parade','Hamilton, 4007',-27.4379013700,153.0642227000),(29,'Hidden World Park','Roghan Rd','Fitzgibbon',-27.3397170100,153.0349810000),(30,'Holland Park Library Wifi','81 Seville Road','Holland Park, 4121',-27.5229228600,153.0722921000),(31,'Inala Library Wifi','Inala Shopping centre, Corsair Ave','Inala, 4077',-27.5982857400,152.9735217000),(32,'Indooroopilly Library Wifi','Indrooroopilly Shopping centre, Level 4, 322 Moggill Road','Indooroopilly, 4068',-27.4976428700,152.9736471000),(33,'Kalinga Park','Kalinga St','Clayfield',-27.4066600000,153.0521700000),(34,'Kenmore Library Wifi','Kenmore Village Shopping Centre, Brookfield Road','Kenmore, 4069',-27.5059285200,152.9386437000),(35,'King Edward Park (Jacob\'s Ladder)','Turbot St and Wickham Tce','Brisbane',-27.4658900000,153.0240600000),(36,'King George Square','Ann & Adelaide Sts','Brisbane',-27.4684300000,153.0242200000),(37,'Mitchelton Library Wifi','37 Helipolis Parada','Mitchelton, 4053',-27.4170416500,152.9783402000),(38,'Mt Coot-tha Botanic Gardens Library Wifi','Administration Building, Brisbane Botanic Gardens (Mt Coot-tha), Mt Coot-tha Road','Toowong, 4066',-27.4752990800,152.9760412000),(39,'Mt Gravatt Library Wifi','8 Creek Road','Mt Gravatt, 4122',-27.5385548200,153.0802628000),(40,'Mt Ommaney Library Wifi','Mt Ommaney Shopping Centre, 171 Dandenong Road','Mt Ommaney, 4074',-27.5482419800,152.9378099000),(41,'New Farm Library Wifi','135 Sydney Street','New Farm, 4005',-27.4673657400,153.0495841000),(42,'New Farm Park Wifi','Brunswick Street','New Farm',-27.4704600000,153.0522300000),(43,'Nundah Library Wifi','1 Bage Street','Nundah, 4012',-27.4012590800,153.0583751000),(44,'Oriel Park','Cnr of Alexandra & Lancaster Rds','Ascot',-27.4292800000,153.0576800000),(45,'Orleigh Park','Hill End Tce','West End',-27.4899500000,153.0032600000),(46,'Post Office Square','Queen & Adelaide Sts','Brisbane',-27.4673500000,153.0273500000),(47,'Rocks Riverside Park','Counihan Rd','Seventeen Mile Rocks',-27.5415300000,152.9591300000),(48,'Sandgate Library Wifi','Seymour Street','Sandgate, 4017',-27.3206052300,153.0704927000),(49,'Stones Corner Library Wifi','280 Logan Road','Stones Corner, 4120',-27.4980357500,153.0436550000),(50,'Sunnybank Hills Library Wifi','Sunnybank Hills Shopping Centre, Corner Compton and Calam Roads','Sunnybank Hills, 4109',-27.6109253000,153.0550706000),(51,'Teralba Park','Pullen & Osborne Rds','Everton Park',-27.4028600000,152.9805900000),(52,'Toowong Library Wifi','Toowon Village Shopping Centre, Sherwood Road','Toowong, 4066',-27.4850511600,152.9925091000),(53,'West End Library Wifi','178 - 180 Boundary Street','West End, 4101',-27.4824507800,153.0120763000),(54,'Wynnum Library Wifi','Wynnum Civic Centre, 66 Bay Terrace','Wynnum, 4178',-27.4424489400,153.1731968000),(55,'Zillmere Library Wifi','Corner Jennings Street and Zillmere Road','Zillmere, 4034',-27.3601423200,153.0407898000);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `gender` varchar(10) CHARACTER SET utf8 DEFAULT 'male',
  `password` char(128) CHARACTER SET utf8 NOT NULL,
  `salt` char(128) NOT NULL,
  `sendmail` bit(1) DEFAULT b'0',
  `dob` varchar(10) DEFAULT NULL,
  `phone` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (22,'tested','tested@tested.com','male','8c497425d58c3e1d3212e7f1f7d3afc4580e0a3ca2e64540f86d6be9df3601ec','365630368574706d9f23788.56721115','','21/05/1994','07 12341234');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `reviewID` int(11) NOT NULL AUTO_INCREMENT,
  `locationID` int(11) NOT NULL,
  `postingUID` int(11) NOT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) DEFAULT NULL,
  `textContent` varchar(400) DEFAULT NULL,
  `Rating` int(1) DEFAULT NULL,
  PRIMARY KEY (`reviewID`),
  KEY `userID_idx` (`postingUID`),
  KEY `locationID_idx` (`locationID`),
  CONSTRAINT `locationID` FOREIGN KEY (`locationID`) REFERENCES `items` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `postingUID` FOREIGN KEY (`postingUID`) REFERENCES `members` (`userID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (14,27,22,'2016-05-27 01:15:54','heres a review','review is here',5);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'hotspots'
--

--
-- Dumping routines for database 'hotspots'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-27  1:22:25
