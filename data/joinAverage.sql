SELECT * FROM (SELECT AVG(coalesce(reviews.Rating)) as avgrating, items.*
FROM items
LEFT JOIN reviews ON items.ID = reviews.locationID
GROUP BY items.ID) AS tmp_table
ORDER BY tmp_table.ID